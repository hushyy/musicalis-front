import { Component, OnInit } from '@angular/core';
import { Profil } from 'app/entities/entity';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { ProfilService } from 'app/services/profil.service';
import { LoginService } from 'app/services/login.service';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  profil: Profil;
  connected: boolean = false;
  id: string;

  nom: string;
  prenom: string;
  username: string = '';
  password: string = '';

  constructor(private profilService: ProfilService, private loginService: LoginService) {
    this.loginService.subscribe((id) => {this.id = id});
  }

  ngOnInit() {
  }

  getProfil() {
    this.profilService.getProfil(this.id)
    .subscribe((user) =>{ 
      console.log('user', user);   
      if (user.connecte) {
        this.profil = user;
        this.connected = true;
      }  
    });
  }

}
