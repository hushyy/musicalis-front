const environmentVariables = {};


export function restUrl() {
    return (
      environmentVariables['musicalisServer'] || 'http://localhost:8080/'
    ) + (
      environmentVariables['wsPath'] || 'rest/services'
    );
}