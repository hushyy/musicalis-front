/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AppelWebServiceService } from './appel-web-service.service';

describe('AppelWebServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppelWebServiceService]
    });
  });

  it('should ...', inject([AppelWebServiceService], (service: AppelWebServiceService) => {
    expect(service).toBeTruthy();
  }));
});
