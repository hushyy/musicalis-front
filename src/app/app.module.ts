import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { CarnetContactComponent } from './carnet-contact/carnet-contact.component';
import { BandeauFrontComponent } from './bandeau-front/bandeau-front.component';
import { ListeMenusComponent } from './liste-menus/liste-menus.component';


import { ProfilComponent } from './profil/profil.component';
import { LoginComponent } from './login/login.component';
import { routings } from 'app/app.routing';
import { LoginService } from 'app/services/login.service';
import { ProfilService } from 'app/services/profil.service';
import { HttpService } from 'app/services/http.service';



@NgModule({
  declarations: [
    AppComponent,
    CarnetContactComponent,
    BandeauFrontComponent,
    ListeMenusComponent,
    ProfilComponent,
    LoginComponent
  ],
  imports: [
    routings,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [LoginService, ProfilService],
  bootstrap: [AppComponent]
})
export class AppModule { }
