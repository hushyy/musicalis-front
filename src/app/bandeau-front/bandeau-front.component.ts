import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/login.service';

@Component({
  selector: 'app-bandeau-front',
  templateUrl: './bandeau-front.component.html',
  styleUrls: ['./bandeau-front.component.css']
})
export class BandeauFrontComponent implements OnInit {

  constructor(private loginservice: LoginService) {
    // loginservice.
   }

  ngOnInit() {
  }

  authentificated(): boolean {
    return true;
  }

  logged() {
    return true;
  }
}
