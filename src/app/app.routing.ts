import { RouterModule, Routes } from '@angular/router';
import { ProfilComponent } from 'app/profil/profil.component';
import { CarnetContactComponent } from 'app/carnet-contact/carnet-contact.component';

const appRoutes: Routes = [
    { path: 'carnet', component: CarnetContactComponent },
    { path: 'profil', component: ProfilComponent}
  ];

export const routings = RouterModule.forRoot(appRoutes)