export class LoginUser {
    username: string;
    pwd?: string;
    nom?: string;
    prenom?: string;
    errorLog: boolean;
}

export class Profil {
    errorLog: boolean;
    login: string;
    passeword: string;
    nom: string;
    prenom: string;
    dateNaissance?: Date;
    dateInscription?: Date;
    adresse?: string;
    codePostal?: string;
    ville?: string;
    connecte?: boolean;
    mail?: string;
    avatar?: string;
    description?:string;
    id: string;
}