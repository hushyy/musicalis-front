import { Component, OnInit } from '@angular/core';
import { Profil } from 'app/entities/entity';
import { LoginService } from 'app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser: Profil;
  connected: boolean = false;

  nom: string;
  prenom: string;
  username: string = '';
  password: string = '';

  constructor(private loginService: LoginService) {
    
  }

  ngOnInit() {
  }

  login() {
    console.log('on envoie');
    this.loginService.getLoginInformation(this.username, this.password)
    .subscribe((user) =>{ 
      console.log('user', user);   
      if (user.connecte) {
        this.loginUser = user;
        this.connected = true;
      }  
    });
  }

  

}
