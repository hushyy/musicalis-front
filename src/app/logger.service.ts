
const moment = require('moment');
const winston = require('winston');
// const Rotate = require('winston-logrotate').Rotate;
const path = require('path');
let transport = new (winston.transports.Console)();


const timeFormatFn = function () {
    moment.locale('fr');
    return moment().format('DD-MM-YYYY HH:mm:ss');
};

const logger = new winston.Logger({
    level: 'error',
    transports: [transport]
});

export function info(msg: string): void {
    logger.info(msg);
}
export function debug(msg: string): void {
    logger.debug(msg);
}
export function error(msg: string): void {
    logger.error(msg);
}