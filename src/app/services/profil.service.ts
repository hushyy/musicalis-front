import { Injectable } from '@angular/core';
import { Profil } from 'app/entities/entity';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { HttpService } from 'app/services/http.service';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { EventEmitter } from 'events';

@Injectable()
export class ProfilService {
    
    user: Profil;
    idUser = new EventEmitter();
    
      constructor(public httpService: Http) { }
    
      getProfil(id: string): Observable<Profil> {
        let args: Profil;
        args.id = id
        return this.httpService.get('http://192.168.0.30:8080/Musicalis/musicalis/rest/profil?id='+id)
        .map((response) => response.json())
        .do(user => {this.user = user});
        
      }
}