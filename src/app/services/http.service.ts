import { Injectable } from '@angular/core';
import { restUrl } from './../environnement';
import { debug, error } from 'app/logger.service';
import { Response, Request, RequestOptionsArgs, Http, RequestOptions, ConnectionBackend } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Interceptor } from './interceptor';
import "rxjs/add/operator/map";


const HTTP_ENDPOINT_LOGIN = 'login';

const request: any = require('request');

@Injectable()
export class HttpService extends Http{

  constructor(backend: ConnectionBackend, options: RequestOptions) {
    super(backend, options);
  }

  request(url: string | Request, options?: RequestOptions): Observable<Response> {
    return super.request(url, options)
                .map(response => response.json())
                .map(response => {
                  console.log(response);
                  return response;
                })
  }


  public async askServer(service: string, params: any) {
    const webserviceUrl = restUrl() + '/' + service;

      debug(webserviceUrl + ' avec' + JSON.stringify(params));
      let responseServer;
      try {
        responseServer = await request.getAsync({
          url: webserviceUrl,
          qs: params,
         /* auth: {
            user: auth.user.username,
            pass: auth.user.password
          }*/
        });
      }catch (erreur) {
        error(erreur);
        return Promise.reject(erreur);
      }
      if (responseServer.statusCode === 200) {
        if (!responseServer.body) {
          return;
        }
        try {
          return JSON.parse(responseServer.body);
        } catch (e) {
          alert('imposible de lire de retour de iTex suivant : \n' + responseServer.body);
          return;
        }
      }


  }

}
