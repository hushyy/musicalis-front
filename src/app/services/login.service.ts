import { Injectable, EventEmitter } from '@angular/core';
import { LoginUser, Profil } from 'app/entities/entity';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { HttpService } from 'app/services/http.service';
import { Observable } from 'rxjs/Observable';
import { RequestOptions, Http } from '@angular/http';

@Injectable()
export class LoginService {

  user: Profil;
  idUser = new EventEmitter<String>();

  constructor(public httpService: Http) { }

  getLoginInformation(username: string, pwd: string): Observable<Profil> {
    let args: Profil;
    args.mail = username;
    args.passeword = pwd;
    return this.httpService.post('http://192.168.0.30:8080/Musicalis/musicalis/rest/profil', args)
    .map((response) => response.json())
    .do(user => {this.user = user});
    
  }

  emitIdUser() {
    this.idUser.emit(this.user.id);
}

  subscribeToIdUser() {
    this.idUser.subscribe();
  }
}
