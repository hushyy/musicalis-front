import { MusicalisFrontPage } from './app.po';

describe('musicalis-front App', function() {
  let page: MusicalisFrontPage;

  beforeEach(() => {
    page = new MusicalisFrontPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
